```
Rahadian Suryo Prayitno - 5025201149 <br>
Christian J H Pakpahan - 50252021153
```


**PROJECT REPORT**

**No. 1**
Pada suatu hari, Han dan teman-temannya diberikan tugas untuk mencari foto. Namun,
karena laptop teman-temannya rusak ternyata tidak bisa dipakai karena rusak, Han dengan
senang hati memperbolehkan teman-temannya untuk meminjam laptopnya. Untuk
mempermudah pekerjaan mereka, Han membuat sebuah program.

**1REGISTER**
a. Han membuat sistem register pada script register.sh dan setiap user yang berhasil
didaftarkan disimpan di dalam file ./users/user.txt. Han juga membuat sistem login
yang dibuat di script main.sh
b. Demi menjaga keamanan, input password pada login dan register harus
tertutup/hidden dan password yang didaftarkan memiliki kriteria sebagai berikut
i.
ii.
iii.
Minimal 8 karakter
Memiliki minimal 1 huruf kapital dan 1 huruf kecil
Alphanumeric
iv.
Tidak boleh sama dengan username


```
#/bin/bash


echo -ne "Username : "
read username
echo -ne "Password : "
read -s password


if [ -z $username ] || [ -z $password ] ;then
    echo ""
    echo "Error : Masukan Username dan Password dengan Benar!!"
else
    x=$( echo $password | wc -m )
    if [ $password == $username ]; then
        echo -e "\nError : Password Tidak Boleh Sama Dengan Username!!"
    else
        if [ $x -gt 8 ]; then
            if [[ $password =~ [A-Z] && $password =~ [a-z] ]]; then
                if [[ $password =~ [^a-zA-Z0-9] ]] ; then
                    echo -e "Username : $username \nPassword : $password" > login.txt
                    echo -e "\nLogin Success\nFile Log : login.txt"
                else
                    echo -e "\nError : Passwords at least alphanumeric!!"
                fi
            else
                echo -e '\nError : Password Must At Least Contain Capital Letters and LowerCase Letters!!'
            fi
        else
            echo -e "\nError : Password must more than 8 character !!"
        fi
    fi
fi

        echo "$username $password"  >> ./users/user.txt
        echo ""
         printf '%(%m/%d/%y %H:%M:%S)T ' >> log.txt
        printf "REGISTER: INFO User %s registered successfully\n" $username >> log.txt
```


**Output register**
![](https://gitlab.com/rahadianprayitno13/aan/-/raw/main/WhatsApp_Image_2022-03-06_at_9.31.13_PM.jpeg)

**Output ketika password benar**
![](https://gitlab.com/rahadianprayitno13/aan/-/raw/main/WhatsApp_Image_2022-03-06_at_9.34.35_PM.jpeg)

**Output ketika password salah**
![](https://gitlab.com/rahadianprayitno13/aan/-/raw/main/WhatsApp_Image_2022-03-06_at_9.35.31_PM.jpeg)

**Output registrasi berhasil**
![](https://gitlab.com/rahadianprayitno13/aan/-/raw/main/WhatsApp_Image_2022-03-06_at_9.37.07_PM.jpeg)

**Riwayat registrasi**
![](https://gitlab.com/rahadianprayitno13/aan/-/raw/main/WhatsApp_Image_2022-03-06_at_9.38.46_PM.jpeg)


**No.2**
Pada tanggal 22 Januari 2022, website https://daffa.info di hack oleh seseorang yang tidak
bertanggung jawab. Sehingga hari sabtu yang seharusnya hari libur menjadi berantakan.
Dapos langsung membuka log website dan menemukan banyak request yang berbahaya.
Bantulah Dapos untuk membaca log website https://daffa.info Buatlah sebuah script awk
bernama "soal2_forensic_dapos.sh" untuk melaksanakan tugas-tugas berikut:

**2B**
Dikarenakan serangan yang diluncurkan ke website https://daffa.info sangat banyak,
Dapos ingin tahu berapa rata-rata request per jam yang dikirimkan penyerang ke
website. Kemudian masukkan jumlah rata-ratanya ke dalam sebuah file bernama
ratarata.txt ke dalam folder yang sudah dibuat sebelumnya.

```
#2B
cat log_website_daffainfo.log | awk 'BEGIN{count=0} {count++} END{print "Rata-rata serangan adalah sebanyak "(count-1)/12 "  request per jam"}' >> ratarata.txt
```


**Output 2B**
![](https://gitlab.com/rahadianprayitno13/aan/-/raw/main/result.png)



**2D**
Beberapa request ada yang menggunakan user-agent ada yang tidak. Dari banyaknya
request, berapa banyak requests yang menggunakan user-agent curl?
Kemudian masukkan berapa banyak requestnya kedalam file bernama result.txt yang
telah dibuat sebelumnya.

```
#2D
cat log_website_daffainfo.log | awk -v str="curl" '{ c += gsub(str, str) } END { print "Ada " c " requests yang menggunakan curl sebagai user-agent" }'  >> result.txt
```


**Output 2D**
![](https://gitlab.com/rahadianprayitno13/aan/-/raw/main/ratarata.png)


